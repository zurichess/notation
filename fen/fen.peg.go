package fen

import (
	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/board"
	"fmt"
	"math"
	"sort"
	"strconv"
)

const endSymbol rune = 1114112

/* The rule types inferred from the grammar are below. */
type pegRule uint8

const (
	ruleUnknown pegRule = iota
	rulefen
	ruleinit
	rulepiecePlacement
	rulesideToMove
	rulecastlingAbility
	ruleenPassantSquare
	rulehalfmoveClock
	rulefullmoveCounter
	ruleNUMBER
	ruleSPACE
	ruleSQUARE
	ruleAction0
	ruleAction1
	rulePegText
	ruleAction2
	ruleAction3
	ruleAction4
	ruleAction5
	ruleAction6
	ruleAction7
)

var rul3s = [...]string{
	"Unknown",
	"fen",
	"init",
	"piecePlacement",
	"sideToMove",
	"castlingAbility",
	"enPassantSquare",
	"halfmoveClock",
	"fullmoveCounter",
	"NUMBER",
	"SPACE",
	"SQUARE",
	"Action0",
	"Action1",
	"PegText",
	"Action2",
	"Action3",
	"Action4",
	"Action5",
	"Action6",
	"Action7",
}

type token32 struct {
	pegRule
	begin, end uint32
}

func (t *token32) String() string {
	return fmt.Sprintf("\x1B[34m%v\x1B[m %v %v", rul3s[t.pegRule], t.begin, t.end)
}

type node32 struct {
	token32
	up, next *node32
}

func (node *node32) print(pretty bool, buffer string) {
	var print func(node *node32, depth int)
	print = func(node *node32, depth int) {
		for node != nil {
			for c := 0; c < depth; c++ {
				fmt.Printf(" ")
			}
			rule := rul3s[node.pegRule]
			quote := strconv.Quote(string(([]rune(buffer)[node.begin:node.end])))
			if !pretty {
				fmt.Printf("%v %v\n", rule, quote)
			} else {
				fmt.Printf("\x1B[34m%v\x1B[m %v\n", rule, quote)
			}
			if node.up != nil {
				print(node.up, depth+1)
			}
			node = node.next
		}
	}
	print(node, 0)
}

func (node *node32) Print(buffer string) {
	node.print(false, buffer)
}

func (node *node32) PrettyPrint(buffer string) {
	node.print(true, buffer)
}

type tokens32 struct {
	tree []token32
}

func (t *tokens32) Trim(length uint32) {
	t.tree = t.tree[:length]
}

func (t *tokens32) Print() {
	for _, token := range t.tree {
		fmt.Println(token.String())
	}
}

func (t *tokens32) AST() *node32 {
	type element struct {
		node *node32
		down *element
	}
	tokens := t.Tokens()
	var stack *element
	for _, token := range tokens {
		if token.begin == token.end {
			continue
		}
		node := &node32{token32: token}
		for stack != nil && stack.node.begin >= token.begin && stack.node.end <= token.end {
			stack.node.next = node.up
			node.up = stack.node
			stack = stack.down
		}
		stack = &element{node: node, down: stack}
	}
	if stack != nil {
		return stack.node
	}
	return nil
}

func (t *tokens32) PrintSyntaxTree(buffer string) {
	t.AST().Print(buffer)
}

func (t *tokens32) PrettyPrintSyntaxTree(buffer string) {
	t.AST().PrettyPrint(buffer)
}

func (t *tokens32) Add(rule pegRule, begin, end, index uint32) {
	if tree := t.tree; int(index) >= len(tree) {
		expanded := make([]token32, 2*len(tree))
		copy(expanded, tree)
		t.tree = expanded
	}
	t.tree[index] = token32{
		pegRule: rule,
		begin:   begin,
		end:     end,
	}
}

func (t *tokens32) Tokens() []token32 {
	return t.tree
}

type pegFEN struct {
	epd *notation.EPD
	err error

	Buffer string
	buffer []rune
	rules  [21]func() bool
	parse  func(rule ...int) error
	reset  func()
	Pretty bool
	tokens32
}

func (p *pegFEN) Parse(rule ...int) error {
	return p.parse(rule...)
}

func (p *pegFEN) Reset() {
	p.reset()
}

type textPosition struct {
	line, symbol int
}

type textPositionMap map[int]textPosition

func translatePositions(buffer []rune, positions []int) textPositionMap {
	length, translations, j, line, symbol := len(positions), make(textPositionMap, len(positions)), 0, 1, 0
	sort.Ints(positions)

search:
	for i, c := range buffer {
		if c == '\n' {
			line, symbol = line+1, 0
		} else {
			symbol++
		}
		if i == positions[j] {
			translations[positions[j]] = textPosition{line, symbol}
			for j++; j < length; j++ {
				if i != positions[j] {
					continue search
				}
			}
			break search
		}
	}

	return translations
}

type parseError struct {
	p   *pegFEN
	max token32
}

func (e *parseError) Error() string {
	tokens, error := []token32{e.max}, "\n"
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(e.p.buffer, positions)
	format := "parse error near %v (line %v symbol %v - line %v symbol %v):\n%v\n"
	if e.p.Pretty {
		format = "parse error near \x1B[34m%v\x1B[m (line %v symbol %v - line %v symbol %v):\n%v\n"
	}
	for _, token := range tokens {
		begin, end := int(token.begin), int(token.end)
		error += fmt.Sprintf(format,
			rul3s[token.pegRule],
			translations[begin].line, translations[begin].symbol,
			translations[end].line, translations[end].symbol,
			strconv.Quote(string(e.p.buffer[begin:end])))
	}

	return error
}

func (p *pegFEN) PrintSyntaxTree() {
	if p.Pretty {
		p.tokens32.PrettyPrintSyntaxTree(p.Buffer)
	} else {
		p.tokens32.PrintSyntaxTree(p.Buffer)
	}
}

func (p *pegFEN) Execute() {
	buffer, _buffer, text, begin, end := p.Buffer, p.buffer, "", 0, 0
	for _, token := range p.Tokens() {
		switch token.pegRule {

		case rulePegText:
			begin, end = int(token.begin), int(token.end)
			text = string(_buffer[begin:end])

		case ruleAction0:

		case ruleAction1:

			p.epd = &notation.EPD{Position: board.NewPosition()}

		case ruleAction2:

			if err := board.ParsePiecePlacement(text, p.epd.Position); err != nil {
				p.err = err
				return
			}

		case ruleAction3:

			if err := board.ParseSideToMove(text, p.epd.Position); err != nil {
				p.err = err
				return
			}

		case ruleAction4:

			if err := board.ParseCastlingAbility(text, p.epd.Position); err != nil {
				p.err = err
				return
			}

		case ruleAction5:

			if err := board.ParseEnpassantSquare(text, p.epd.Position); err != nil {
				p.err = err
				return
			}

		case ruleAction6:

			if n, err := strconv.Atoi(text); err != nil {
				p.err = err
				return
			} else {
				p.epd.Position.SetHalfmoveClock(n)
			}

		case ruleAction7:

			if n, err := strconv.Atoi(text); err != nil {
				p.err = err
				return
			} else {
				p.epd.Position.SetFullmoveCounter(n)
			}

		}
	}
	_, _, _, _, _ = buffer, _buffer, text, begin, end
}

func (p *pegFEN) Init() {
	var (
		max                  token32
		position, tokenIndex uint32
		buffer               []rune
	)
	p.reset = func() {
		max = token32{}
		position, tokenIndex = 0, 0

		p.buffer = []rune(p.Buffer)
		if len(p.buffer) == 0 || p.buffer[len(p.buffer)-1] != endSymbol {
			p.buffer = append(p.buffer, endSymbol)
		}
		buffer = p.buffer
	}
	p.reset()

	_rules := p.rules
	tree := tokens32{tree: make([]token32, math.MaxInt16)}
	p.parse = func(rule ...int) error {
		r := 1
		if len(rule) > 0 {
			r = rule[0]
		}
		matches := p.rules[r]()
		p.tokens32 = tree
		if matches {
			p.Trim(tokenIndex)
			return nil
		}
		return &parseError{p, max}
	}

	add := func(rule pegRule, begin uint32) {
		tree.Add(rule, begin, position, tokenIndex)
		tokenIndex++
		if begin != position && position > max.end {
			max = token32{rule, begin, position}
		}
	}

	matchDot := func() bool {
		if buffer[position] != endSymbol {
			position++
			return true
		}
		return false
	}

	/*matchChar := func(c byte) bool {
		if buffer[position] == c {
			position++
			return true
		}
		return false
	}*/

	/*matchRange := func(lower byte, upper byte) bool {
		if c := buffer[position]; c >= lower && c <= upper {
			position++
			return true
		}
		return false
	}*/

	_rules = [...]func() bool{
		nil,
		/* 0 fen <- <(init piecePlacement SPACE sideToMove SPACE castlingAbility SPACE enPassantSquare SPACE halfmoveClock SPACE fullmoveCounter !. Action0)> */
		func() bool {
			position0, tokenIndex0 := position, tokenIndex
			{
				position1 := position
				if !_rules[ruleinit]() {
					goto l0
				}
				if !_rules[rulepiecePlacement]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[rulesideToMove]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[rulecastlingAbility]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[ruleenPassantSquare]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[rulehalfmoveClock]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[rulefullmoveCounter]() {
					goto l0
				}
				{
					position2, tokenIndex2 := position, tokenIndex
					if !matchDot() {
						goto l2
					}
					goto l0
				l2:
					position, tokenIndex = position2, tokenIndex2
				}
				if !_rules[ruleAction0]() {
					goto l0
				}
				add(rulefen, position1)
			}
			return true
		l0:
			position, tokenIndex = position0, tokenIndex0
			return false
		},
		/* 1 init <- <Action1> */
		func() bool {
			position3, tokenIndex3 := position, tokenIndex
			{
				position4 := position
				if !_rules[ruleAction1]() {
					goto l3
				}
				add(ruleinit, position4)
			}
			return true
		l3:
			position, tokenIndex = position3, tokenIndex3
			return false
		},
		/* 2 piecePlacement <- <(<([1-8] / 'p' / 'n' / 'b' / 'r' / 'q' / 'k' / 'P' / 'N' / 'B' / 'R' / 'Q' / 'K' / '/')+> Action2)> */
		func() bool {
			position5, tokenIndex5 := position, tokenIndex
			{
				position6 := position
				{
					position7 := position
					{
						position10, tokenIndex10 := position, tokenIndex
						if c := buffer[position]; c < rune('1') || c > rune('8') {
							goto l11
						}
						position++
						goto l10
					l11:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('p') {
							goto l12
						}
						position++
						goto l10
					l12:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('n') {
							goto l13
						}
						position++
						goto l10
					l13:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('b') {
							goto l14
						}
						position++
						goto l10
					l14:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('r') {
							goto l15
						}
						position++
						goto l10
					l15:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('q') {
							goto l16
						}
						position++
						goto l10
					l16:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('k') {
							goto l17
						}
						position++
						goto l10
					l17:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('P') {
							goto l18
						}
						position++
						goto l10
					l18:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('N') {
							goto l19
						}
						position++
						goto l10
					l19:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('B') {
							goto l20
						}
						position++
						goto l10
					l20:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('R') {
							goto l21
						}
						position++
						goto l10
					l21:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('Q') {
							goto l22
						}
						position++
						goto l10
					l22:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('K') {
							goto l23
						}
						position++
						goto l10
					l23:
						position, tokenIndex = position10, tokenIndex10
						if buffer[position] != rune('/') {
							goto l5
						}
						position++
					}
				l10:
				l8:
					{
						position9, tokenIndex9 := position, tokenIndex
						{
							position24, tokenIndex24 := position, tokenIndex
							if c := buffer[position]; c < rune('1') || c > rune('8') {
								goto l25
							}
							position++
							goto l24
						l25:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('p') {
								goto l26
							}
							position++
							goto l24
						l26:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('n') {
								goto l27
							}
							position++
							goto l24
						l27:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('b') {
								goto l28
							}
							position++
							goto l24
						l28:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('r') {
								goto l29
							}
							position++
							goto l24
						l29:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('q') {
								goto l30
							}
							position++
							goto l24
						l30:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('k') {
								goto l31
							}
							position++
							goto l24
						l31:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('P') {
								goto l32
							}
							position++
							goto l24
						l32:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('N') {
								goto l33
							}
							position++
							goto l24
						l33:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('B') {
								goto l34
							}
							position++
							goto l24
						l34:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('R') {
								goto l35
							}
							position++
							goto l24
						l35:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('Q') {
								goto l36
							}
							position++
							goto l24
						l36:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('K') {
								goto l37
							}
							position++
							goto l24
						l37:
							position, tokenIndex = position24, tokenIndex24
							if buffer[position] != rune('/') {
								goto l9
							}
							position++
						}
					l24:
						goto l8
					l9:
						position, tokenIndex = position9, tokenIndex9
					}
					add(rulePegText, position7)
				}
				if !_rules[ruleAction2]() {
					goto l5
				}
				add(rulepiecePlacement, position6)
			}
			return true
		l5:
			position, tokenIndex = position5, tokenIndex5
			return false
		},
		/* 3 sideToMove <- <(<('w' / 'b')> Action3)> */
		func() bool {
			position38, tokenIndex38 := position, tokenIndex
			{
				position39 := position
				{
					position40 := position
					{
						position41, tokenIndex41 := position, tokenIndex
						if buffer[position] != rune('w') {
							goto l42
						}
						position++
						goto l41
					l42:
						position, tokenIndex = position41, tokenIndex41
						if buffer[position] != rune('b') {
							goto l38
						}
						position++
					}
				l41:
					add(rulePegText, position40)
				}
				if !_rules[ruleAction3]() {
					goto l38
				}
				add(rulesideToMove, position39)
			}
			return true
		l38:
			position, tokenIndex = position38, tokenIndex38
			return false
		},
		/* 4 castlingAbility <- <(<('-' / ('K' / 'k' / 'Q' / 'q')*)> Action4)> */
		func() bool {
			position43, tokenIndex43 := position, tokenIndex
			{
				position44 := position
				{
					position45 := position
					{
						position46, tokenIndex46 := position, tokenIndex
						if buffer[position] != rune('-') {
							goto l47
						}
						position++
						goto l46
					l47:
						position, tokenIndex = position46, tokenIndex46
					l48:
						{
							position49, tokenIndex49 := position, tokenIndex
							{
								position50, tokenIndex50 := position, tokenIndex
								if buffer[position] != rune('K') {
									goto l51
								}
								position++
								goto l50
							l51:
								position, tokenIndex = position50, tokenIndex50
								if buffer[position] != rune('k') {
									goto l52
								}
								position++
								goto l50
							l52:
								position, tokenIndex = position50, tokenIndex50
								if buffer[position] != rune('Q') {
									goto l53
								}
								position++
								goto l50
							l53:
								position, tokenIndex = position50, tokenIndex50
								if buffer[position] != rune('q') {
									goto l49
								}
								position++
							}
						l50:
							goto l48
						l49:
							position, tokenIndex = position49, tokenIndex49
						}
					}
				l46:
					add(rulePegText, position45)
				}
				if !_rules[ruleAction4]() {
					goto l43
				}
				add(rulecastlingAbility, position44)
			}
			return true
		l43:
			position, tokenIndex = position43, tokenIndex43
			return false
		},
		/* 5 enPassantSquare <- <(<('-' / SQUARE)> Action5)> */
		func() bool {
			position54, tokenIndex54 := position, tokenIndex
			{
				position55 := position
				{
					position56 := position
					{
						position57, tokenIndex57 := position, tokenIndex
						if buffer[position] != rune('-') {
							goto l58
						}
						position++
						goto l57
					l58:
						position, tokenIndex = position57, tokenIndex57
						if !_rules[ruleSQUARE]() {
							goto l54
						}
					}
				l57:
					add(rulePegText, position56)
				}
				if !_rules[ruleAction5]() {
					goto l54
				}
				add(ruleenPassantSquare, position55)
			}
			return true
		l54:
			position, tokenIndex = position54, tokenIndex54
			return false
		},
		/* 6 halfmoveClock <- <(NUMBER Action6)> */
		func() bool {
			position59, tokenIndex59 := position, tokenIndex
			{
				position60 := position
				if !_rules[ruleNUMBER]() {
					goto l59
				}
				if !_rules[ruleAction6]() {
					goto l59
				}
				add(rulehalfmoveClock, position60)
			}
			return true
		l59:
			position, tokenIndex = position59, tokenIndex59
			return false
		},
		/* 7 fullmoveCounter <- <(NUMBER Action7)> */
		func() bool {
			position61, tokenIndex61 := position, tokenIndex
			{
				position62 := position
				if !_rules[ruleNUMBER]() {
					goto l61
				}
				if !_rules[ruleAction7]() {
					goto l61
				}
				add(rulefullmoveCounter, position62)
			}
			return true
		l61:
			position, tokenIndex = position61, tokenIndex61
			return false
		},
		/* 8 NUMBER <- <<('0' / ([1-9] [0-9]*))>> */
		func() bool {
			position63, tokenIndex63 := position, tokenIndex
			{
				position64 := position
				{
					position65 := position
					{
						position66, tokenIndex66 := position, tokenIndex
						if buffer[position] != rune('0') {
							goto l67
						}
						position++
						goto l66
					l67:
						position, tokenIndex = position66, tokenIndex66
						if c := buffer[position]; c < rune('1') || c > rune('9') {
							goto l63
						}
						position++
					l68:
						{
							position69, tokenIndex69 := position, tokenIndex
							if c := buffer[position]; c < rune('0') || c > rune('9') {
								goto l69
							}
							position++
							goto l68
						l69:
							position, tokenIndex = position69, tokenIndex69
						}
					}
				l66:
					add(rulePegText, position65)
				}
				add(ruleNUMBER, position64)
			}
			return true
		l63:
			position, tokenIndex = position63, tokenIndex63
			return false
		},
		/* 9 SPACE <- <' '+> */
		func() bool {
			position70, tokenIndex70 := position, tokenIndex
			{
				position71 := position
				if buffer[position] != rune(' ') {
					goto l70
				}
				position++
			l72:
				{
					position73, tokenIndex73 := position, tokenIndex
					if buffer[position] != rune(' ') {
						goto l73
					}
					position++
					goto l72
				l73:
					position, tokenIndex = position73, tokenIndex73
				}
				add(ruleSPACE, position71)
			}
			return true
		l70:
			position, tokenIndex = position70, tokenIndex70
			return false
		},
		/* 10 SQUARE <- <([a-h] [1-8])> */
		func() bool {
			position74, tokenIndex74 := position, tokenIndex
			{
				position75 := position
				if c := buffer[position]; c < rune('a') || c > rune('h') {
					goto l74
				}
				position++
				if c := buffer[position]; c < rune('1') || c > rune('8') {
					goto l74
				}
				position++
				add(ruleSQUARE, position75)
			}
			return true
		l74:
			position, tokenIndex = position74, tokenIndex74
			return false
		},
		/* 12 Action0 <- <{
		}> */
		func() bool {
			{
				add(ruleAction0, position)
			}
			return true
		},
		/* 13 Action1 <- <{
		        p.epd = &notation.EPD{Position: board.NewPosition()}
		}> */
		func() bool {
			{
				add(ruleAction1, position)
			}
			return true
		},
		nil,
		/* 15 Action2 <- <{
		        if err := board.ParsePiecePlacement(text, p.epd.Position); err != nil {
		                p.err = err
		                return
		        }
		}> */
		func() bool {
			{
				add(ruleAction2, position)
			}
			return true
		},
		/* 16 Action3 <- <{
		        if err := board.ParseSideToMove(text, p.epd.Position); err != nil {
		                p.err = err
		                return
		        }
		}> */
		func() bool {
			{
				add(ruleAction3, position)
			}
			return true
		},
		/* 17 Action4 <- <{
		        if err := board.ParseCastlingAbility(text, p.epd.Position); err != nil {
		                p.err = err
		                return
		        }
		}> */
		func() bool {
			{
				add(ruleAction4, position)
			}
			return true
		},
		/* 18 Action5 <- <{
		        if err := board.ParseEnpassantSquare(text, p.epd.Position); err != nil {
		                p.err = err
		                return
		        }
		}> */
		func() bool {
			{
				add(ruleAction5, position)
			}
			return true
		},
		/* 19 Action6 <- <{
		        if n, err := strconv.Atoi(text); err != nil {
		                p.err = err
		                return
		        } else {
		                p.epd.Position.SetHalfmoveClock(n)
		        }
		}> */
		func() bool {
			{
				add(ruleAction6, position)
			}
			return true
		},
		/* 20 Action7 <- <{
		        if n, err := strconv.Atoi(text); err != nil {
		                p.err = err
		                return
		        } else {
		                p.epd.Position.SetFullmoveCounter(n)
		        }
		}> */
		func() bool {
			{
				add(ruleAction7, position)
			}
			return true
		},
	}
	p.rules = _rules
}
