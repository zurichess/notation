// Copyright 2014-2016 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate peg fen.peg

// Package fen implementes a parser for chess positions in FEN format.
//
// For a description of FEN see http://www.thechessdrum.net/PGN_Reference.txt
package fen

import "bitbucket.org/zurichess/notation"

// Parser implements a parser for chess positions in FEN notation.
type Parser struct{}

func (p *Parser) Parse(line string) (*notation.EPD, error) {
	parser := &pegFEN{Buffer: line}
	parser.Init()
	if err := parser.Parse(); err != nil {
		return nil, err
	}
	parser.Execute()
	if parser.err != nil {
		return nil, parser.err
	}
	return parser.epd, nil
}
