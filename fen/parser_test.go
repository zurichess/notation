// Copyright 2014-2014 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package fen

import (
	"testing"

	. "bitbucket.org/zurichess/board"
)

const fenKiwipete = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1"

func testFENHelper(t *testing.T, expected *Position, fen string) {
	epd, err := (&Parser{}).Parse(fen)
	if err != nil {
		t.Error(err)
		return
	}

	actual := epd.Position
	for sq := SquareMinValue; sq <= SquareMaxValue; sq++ {
		epi := expected.Get(sq)
		api := actual.Get(sq)
		if epi != api {
			t.Errorf("expected %v at %v, got %v", epi, sq, api)
		}
	}
	if expected.Us() != actual.Us() {
		t.Errorf("expected to move %v, got %v",
			expected.Us(), actual.Us())
	}
	if expected.CastlingAbility() != actual.CastlingAbility() {
		t.Errorf("expected CastlingAbility rights %v, got %v",
			expected.CastlingAbility(), actual.CastlingAbility())
	}
	if expected.EnpassantSquare() != actual.EnpassantSquare() {
		t.Errorf("expected EnpassantSquare square %v, got %v",
			expected.EnpassantSquare(), actual.EnpassantSquare())
	}
}

func TestFENStartPosition(t *testing.T) {
	expected := NewPosition()
	expected.Put(SquareA1, WhiteRook)
	expected.Put(SquareB1, WhiteKnight)
	expected.Put(SquareC1, WhiteBishop)
	expected.Put(SquareD1, WhiteQueen)
	expected.Put(SquareE1, WhiteKing)
	expected.Put(SquareF1, WhiteBishop)
	expected.Put(SquareG1, WhiteKnight)
	expected.Put(SquareH1, WhiteRook)

	expected.Put(SquareA8, BlackRook)
	expected.Put(SquareB8, BlackKnight)
	expected.Put(SquareC8, BlackBishop)
	expected.Put(SquareD8, BlackQueen)
	expected.Put(SquareE8, BlackKing)
	expected.Put(SquareF8, BlackBishop)
	expected.Put(SquareG8, BlackKnight)
	expected.Put(SquareH8, BlackRook)

	for f := 0; f < 8; f++ {
		expected.Put(RankFile(1, f), WhitePawn)
		expected.Put(RankFile(6, f), BlackPawn)
	}

	expected.SetSideToMove(White)
	expected.SetCastlingAbility(AnyCastle)
	testFENHelper(t, expected, FENStartPos)
}

func TestHMVCAndFMVN(t *testing.T) {
	data := []struct {
		fen  string
		hmvc int
		fmvn int
	}{
		{"8/p4pk1/1p3p1p/8/2P5/PP3pPP/4rP2/3R1K2 w - - 0 34", 0, 34},
		{"4k3/3R4/1pp1P3/p7/3P4/P1KNn3/8/3r4 w - - 8 43", 8, 43},
		{"rn1qrk2/ppb5/2p1b1pp/8/3P4/5N1P/PPQB1PP1/R3R1K1 w - - 12 1", 12, 1},
	}

	for i, d := range data {
		epd, err := (&Parser{}).Parse(d.fen)
		if err != nil {
			t.Error(err)
			return
		}

		if got := epd.Position.HalfmoveClock(); got != d.hmvc {
			t.Errorf("#%d Got HalfmoveClock() == %d, wanted %d", i, got, d.hmvc)
		}
		if got := epd.Position.FullmoveCounter(); got != d.fmvn {
			t.Errorf("#%d Got FullmoveCounter() == %d, wanted %d", i, got, d.fmvn)
		}
	}
}
