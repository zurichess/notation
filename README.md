zurichess/notation is a library for parsing chess positions.
See documentation at https://godoc.org/bitbucket.org/zurichess/notation.

Currently only positions in EPD and FEN formats are supported,
but game parsing (PGN files) is also be planned.
Good documentation on FEN, EPD and PGN formats can be
found at http://www.thechessdrum.net/PGN_Reference.txt.
