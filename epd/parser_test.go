// Copyright 2014-2016 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package epd

import (
	"testing"

	. "bitbucket.org/zurichess/board"
)

func TestEPDParser(t *testing.T) {
	// An EPD taken from http://www.stmintz.com/ccc/index.php?id=20631
	line := "rnb2r1k/pp2p2p/2pp2p1/q2P1p2/8/1Pb2NP1/PB2PPBP/R2Q1RK1 w - - " +
		"am Nh4 Bh1; bm Qd2 Qe1; fmvn 123; hmvc 15; " +
		"id \"BK.14\"; c8 \" space\ttab\"; c9 \"draw\"; ce -123;"
	epd, err := (&Parser{}).Parse(line)
	if err != nil {
		t.Fatal(err)
	}

	// Verify id.
	expectedID := "BK.14"
	if expectedID != epd.Id {
		t.Fatalf("expected id %s, got %s", expectedID, epd.Id)
	}

	// Verify bm.
	expectedBestMove := []Move{
		MakeMove(Normal, SquareD1, SquareD2, NoPiece, WhiteQueen),
		MakeMove(Normal, SquareD1, SquareE1, NoPiece, WhiteQueen),
	}
	if len(expectedBestMove) != len(epd.BestMove) {
		t.Fatalf("expected 2 best moves, got %d", len(epd.BestMove))
	}
	for i, bm := range expectedBestMove {
		if bm != epd.BestMove[i] {
			t.Errorf("#%d expected best move %v, got %v", i, bm, epd.BestMove[i])
		}
	}
	// Verify am.
	expectedAvoidMove := []Move{
		MakeMove(Normal, SquareF3, SquareH4, NoPiece, WhiteKnight),
		MakeMove(Normal, SquareG2, SquareH1, NoPiece, WhiteBishop),
	}
	if len(expectedAvoidMove) != len(epd.AvoidMove) {
		t.Fatalf("expected 2 avoid moves, got %d", len(epd.AvoidMove))
	}
	for i, am := range expectedAvoidMove {
		if am != epd.AvoidMove[i] {
			t.Errorf("#%d expected avoid move %v, got %v", i, am, epd.AvoidMove[i])
		}
	}
	// Verify ce.
	expectedCE := int32(-123)
	if epd.Evaluation == nil {
		t.Errorf("got no evaluation, expected epd.Evaluation != nil")
	} else if *epd.Evaluation != expectedCE {
		t.Errorf("got *epd.Evaluation == %d, expected %d", *epd.Evaluation, expectedCE)
	}

	// Remaining fields.
	if want, got := 123, epd.Position.FullmoveCounter(); got != want {
		t.Errorf("got fullmove number %d, expected %d", got, want)
	}
	if want, got := 15, epd.Position.HalfmoveClock(); got != want {
		t.Errorf("got halfmove clock %d, expected %d", got, want)
	}
	if " space\ttab" != epd.Comment["c8"] {
		t.Errorf("execpted comment %s, got %s", " space\ttab", epd.Comment["c8"])
	}
	if "draw" != epd.Comment["c9"] {
		t.Errorf("execpted comment %s, got %s", "draw", epd.Comment["c9"])
	}
}

func TestConvertsString(t *testing.T) {
	line := "r3r1k1/ppqb1ppp/8/4p1NQ/8/2P5/PP3PPP/R3R1K1 b - - " +
		"am Bd7-f5; bm Bd7-e6; c0 \"draw\"; ce 1234; " +
		"hmvc 3; fmvn 8; id \"BK.12\";"

	epd, err := (&Parser{}).Parse(line)
	if err != nil {
		t.Fatal(err)
	}

	actual := epd.String()
	if line != actual {
		t.Errorf("invalid string:\n     got: %s\nexpected: %s\n", actual, line)
	}
}

func TestMoveParser(t *testing.T) {
	fen := "1r2r1k1/PpBb1ppp/8/4p1NQ/8/1rB3B1/PP4PP/R3K3 w Q -"
	moves := []struct {
		san string
		lan string
	}{
		{"O-O-O", "Ke1-c1"},
		{"a8Q", "a7-a8Q"},
		{"a8=Q", "a7-a8Q"},
		{"xb8Q", "a7xb8Q"},
		{"a3", "a2-a3"},
		{"aa3", "a2-a3"},
		{"xb3", "a2xb3"},
		{"a2a3", "a2-a3"},
		{"Bd8", "Bc7-d8"},
		{"Bxb8", "Bc7xb8"},
		{"Bgxe5", "Bg3xe5"},
		{"B7xe5", "Bc7xe5"},
		{"Bc3xe5", "Bc3xe5"},
		{"B3a5", "Bc3-a5"},
	}

	for i, m := range moves {
		line := fen + " bm " + m.san + ";"
		epd, err := (&Parser{}).Parse(line)
		if err != nil {
			t.Logf("epd line: %s", line)
			t.Errorf("#%d could not parse %s: %v", i, m.san, err)
			continue
		}

		if len(epd.BestMove) != 1 {
			t.Errorf("#%d got %d moves, expected 1 move", i, len(epd.BestMove))
			continue
		}
		if epd.BestMove[0].String() != m.lan {
			t.Errorf("#%d for %s, got %s, expected %s", i, m.san, epd.BestMove[0], m.lan)
			continue
		}
	}
}
