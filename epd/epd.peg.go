package epd

import (
	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/san"
	"bitbucket.org/zurichess/board"
	"fmt"
	"sort"
	"strconv"
)

const endSymbol rune = 1114112

/* The rule types inferred from the grammar are below. */
type pegRule uint8

const (
	ruleUnknown pegRule = iota
	ruleepd
	ruleinit
	rulepiecePlacement
	rulesideToMove
	rulecastlingAbility
	ruleenPassantSquare
	ruleoperation
	ruleoperationID
	ruleoperationBm
	ruleoperationAm
	ruleoperationFmvn
	ruleoperationHmvc
	ruleoperationComment
	ruleoperationCe
	rulecommentName
	ruleMOVE
	rulePAWN_MOVE
	rulePIECE_MOVE
	ruleCASTLING
	ruleSQUARES
	ruleF
	ruleR
	ruleSPACE
	ruleNUMBER
	ruleTEXT
	ruleAction0
	rulePegText
	ruleAction1
	ruleAction2
	ruleAction3
	ruleAction4
	ruleAction5
	ruleAction6
	ruleAction7
	ruleAction8
	ruleAction9
	ruleAction10
	ruleAction11
	ruleAction12
	ruleAction13
	ruleAction14
	ruleAction15
)

var rul3s = [...]string{
	"Unknown",
	"epd",
	"init",
	"piecePlacement",
	"sideToMove",
	"castlingAbility",
	"enPassantSquare",
	"operation",
	"operationID",
	"operationBm",
	"operationAm",
	"operationFmvn",
	"operationHmvc",
	"operationComment",
	"operationCe",
	"commentName",
	"MOVE",
	"PAWN_MOVE",
	"PIECE_MOVE",
	"CASTLING",
	"SQUARES",
	"F",
	"R",
	"SPACE",
	"NUMBER",
	"TEXT",
	"Action0",
	"PegText",
	"Action1",
	"Action2",
	"Action3",
	"Action4",
	"Action5",
	"Action6",
	"Action7",
	"Action8",
	"Action9",
	"Action10",
	"Action11",
	"Action12",
	"Action13",
	"Action14",
	"Action15",
}

type token32 struct {
	pegRule
	begin, end uint32
}

func (t *token32) String() string {
	return fmt.Sprintf("\x1B[34m%v\x1B[m %v %v", rul3s[t.pegRule], t.begin, t.end)
}

type pegEPD struct {
	epd *notation.EPD
	err error

	number  int
	moves   []board.Move
	text    string
	comment string

	Buffer string
	buffer []rune
	rules  [43]func() bool
	parse  func(rule ...int) error
	reset  func()
	Pretty bool
}

func (p *pegEPD) Parse(rule ...int) error {
	return p.parse(rule...)
}

func (p *pegEPD) Reset() {
	p.reset()
}

type textPosition struct {
	line, symbol int
}

type textPositionMap map[int]textPosition

func translatePositions(buffer []rune, positions []int) textPositionMap {
	length, translations, j, line, symbol := len(positions), make(textPositionMap, len(positions)), 0, 1, 0
	sort.Ints(positions)

search:
	for i, c := range buffer {
		if c == '\n' {
			line, symbol = line+1, 0
		} else {
			symbol++
		}
		if i == positions[j] {
			translations[positions[j]] = textPosition{line, symbol}
			for j++; j < length; j++ {
				if i != positions[j] {
					continue search
				}
			}
			break search
		}
	}

	return translations
}

type parseError struct {
	p   *pegEPD
	max token32
}

func (e *parseError) Error() string {
	tokens, error := []token32{e.max}, "\n"
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(e.p.buffer, positions)
	format := "parse error near %v (line %v symbol %v - line %v symbol %v):\n%v\n"
	if e.p.Pretty {
		format = "parse error near \x1B[34m%v\x1B[m (line %v symbol %v - line %v symbol %v):\n%v\n"
	}
	for _, token := range tokens {
		begin, end := int(token.begin), int(token.end)
		error += fmt.Sprintf(format,
			rul3s[token.pegRule],
			translations[begin].line, translations[begin].symbol,
			translations[end].line, translations[end].symbol,
			strconv.Quote(string(e.p.buffer[begin:end])))
	}

	return error
}

func (p *pegEPD) Init() {
	var (
		max                  token32
		position, tokenIndex uint32
		buffer               []rune
		text                 string
	)
	p.reset = func() {
		max = token32{}
		position, tokenIndex = 0, 0

		p.buffer = []rune(p.Buffer)
		if len(p.buffer) == 0 || p.buffer[len(p.buffer)-1] != endSymbol {
			p.buffer = append(p.buffer, endSymbol)
		}
		buffer = p.buffer
	}
	p.reset()

	_rules := p.rules
	p.parse = func(rule ...int) error {
		r := 1
		if len(rule) > 0 {
			r = rule[0]
		}
		matches := p.rules[r]()
		if matches {
			return nil
		}
		return &parseError{p, max}
	}

	add := func(rule pegRule, begin uint32) {
		tokenIndex++
		if begin != position && position > max.end {
			max = token32{rule, begin, position}
		}
	}

	matchDot := func() bool {
		if buffer[position] != endSymbol {
			position++
			return true
		}
		return false
	}

	/*matchChar := func(c byte) bool {
		if buffer[position] == c {
			position++
			return true
		}
		return false
	}*/

	/*matchRange := func(lower byte, upper byte) bool {
		if c := buffer[position]; c >= lower && c <= upper {
			position++
			return true
		}
		return false
	}*/

	_rules = [...]func() bool{
		nil,
		/* 0 epd <- <(init piecePlacement SPACE sideToMove SPACE castlingAbility SPACE enPassantSquare (SPACE operation ';')* !.)> */
		func() bool {
			position0, tokenIndex0 := position, tokenIndex
			{
				position1 := position
				if !_rules[ruleinit]() {
					goto l0
				}
				if !_rules[rulepiecePlacement]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[rulesideToMove]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[rulecastlingAbility]() {
					goto l0
				}
				if !_rules[ruleSPACE]() {
					goto l0
				}
				if !_rules[ruleenPassantSquare]() {
					goto l0
				}
			l2:
				{
					position3, tokenIndex3 := position, tokenIndex
					if !_rules[ruleSPACE]() {
						goto l3
					}
					if !_rules[ruleoperation]() {
						goto l3
					}
					if buffer[position] != rune(';') {
						goto l3
					}
					position++
					goto l2
				l3:
					position, tokenIndex = position3, tokenIndex3
				}
				{
					position4, tokenIndex4 := position, tokenIndex
					if !matchDot() {
						goto l4
					}
					goto l0
				l4:
					position, tokenIndex = position4, tokenIndex4
				}
				add(ruleepd, position1)
			}
			return true
		l0:
			position, tokenIndex = position0, tokenIndex0
			return false
		},
		/* 1 init <- <Action0> */
		func() bool {
			position5, tokenIndex5 := position, tokenIndex
			{
				position6 := position
				if !_rules[ruleAction0]() {
					goto l5
				}
				add(ruleinit, position6)
			}
			return true
		l5:
			position, tokenIndex = position5, tokenIndex5
			return false
		},
		/* 2 piecePlacement <- <(<([1-8] / 'p' / 'n' / 'b' / 'r' / 'q' / 'k' / 'P' / 'N' / 'B' / 'R' / 'Q' / 'K' / '/')+> Action1)> */
		func() bool {
			position7, tokenIndex7 := position, tokenIndex
			{
				position8 := position
				{
					position9 := position
					{
						position12, tokenIndex12 := position, tokenIndex
						if c := buffer[position]; c < rune('1') || c > rune('8') {
							goto l13
						}
						position++
						goto l12
					l13:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('p') {
							goto l14
						}
						position++
						goto l12
					l14:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('n') {
							goto l15
						}
						position++
						goto l12
					l15:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('b') {
							goto l16
						}
						position++
						goto l12
					l16:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('r') {
							goto l17
						}
						position++
						goto l12
					l17:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('q') {
							goto l18
						}
						position++
						goto l12
					l18:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('k') {
							goto l19
						}
						position++
						goto l12
					l19:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('P') {
							goto l20
						}
						position++
						goto l12
					l20:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('N') {
							goto l21
						}
						position++
						goto l12
					l21:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('B') {
							goto l22
						}
						position++
						goto l12
					l22:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('R') {
							goto l23
						}
						position++
						goto l12
					l23:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('Q') {
							goto l24
						}
						position++
						goto l12
					l24:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('K') {
							goto l25
						}
						position++
						goto l12
					l25:
						position, tokenIndex = position12, tokenIndex12
						if buffer[position] != rune('/') {
							goto l7
						}
						position++
					}
				l12:
				l10:
					{
						position11, tokenIndex11 := position, tokenIndex
						{
							position26, tokenIndex26 := position, tokenIndex
							if c := buffer[position]; c < rune('1') || c > rune('8') {
								goto l27
							}
							position++
							goto l26
						l27:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('p') {
								goto l28
							}
							position++
							goto l26
						l28:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('n') {
								goto l29
							}
							position++
							goto l26
						l29:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('b') {
								goto l30
							}
							position++
							goto l26
						l30:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('r') {
								goto l31
							}
							position++
							goto l26
						l31:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('q') {
								goto l32
							}
							position++
							goto l26
						l32:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('k') {
								goto l33
							}
							position++
							goto l26
						l33:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('P') {
								goto l34
							}
							position++
							goto l26
						l34:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('N') {
								goto l35
							}
							position++
							goto l26
						l35:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('B') {
								goto l36
							}
							position++
							goto l26
						l36:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('R') {
								goto l37
							}
							position++
							goto l26
						l37:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('Q') {
								goto l38
							}
							position++
							goto l26
						l38:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('K') {
								goto l39
							}
							position++
							goto l26
						l39:
							position, tokenIndex = position26, tokenIndex26
							if buffer[position] != rune('/') {
								goto l11
							}
							position++
						}
					l26:
						goto l10
					l11:
						position, tokenIndex = position11, tokenIndex11
					}
					begin := position9
					end := position
					text = string(buffer[begin:end])
				}
				if !_rules[ruleAction1]() {
					goto l7
				}
				add(rulepiecePlacement, position8)
			}
			return true
		l7:
			position, tokenIndex = position7, tokenIndex7
			return false
		},
		/* 3 sideToMove <- <(<('w' / 'b')> Action2)> */
		func() bool {
			position40, tokenIndex40 := position, tokenIndex
			{
				position41 := position
				{
					position42 := position
					{
						position43, tokenIndex43 := position, tokenIndex
						if buffer[position] != rune('w') {
							goto l44
						}
						position++
						goto l43
					l44:
						position, tokenIndex = position43, tokenIndex43
						if buffer[position] != rune('b') {
							goto l40
						}
						position++
					}
				l43:
					begin := position42
					end := position
					text = string(buffer[begin:end])
				}
				if !_rules[ruleAction2]() {
					goto l40
				}
				add(rulesideToMove, position41)
			}
			return true
		l40:
			position, tokenIndex = position40, tokenIndex40
			return false
		},
		/* 4 castlingAbility <- <(<('-' / ('K' / 'k' / 'Q' / 'q')*)> Action3)> */
		func() bool {
			position45, tokenIndex45 := position, tokenIndex
			{
				position46 := position
				{
					position47 := position
					{
						position48, tokenIndex48 := position, tokenIndex
						if buffer[position] != rune('-') {
							goto l49
						}
						position++
						goto l48
					l49:
						position, tokenIndex = position48, tokenIndex48
					l50:
						{
							position51, tokenIndex51 := position, tokenIndex
							{
								position52, tokenIndex52 := position, tokenIndex
								if buffer[position] != rune('K') {
									goto l53
								}
								position++
								goto l52
							l53:
								position, tokenIndex = position52, tokenIndex52
								if buffer[position] != rune('k') {
									goto l54
								}
								position++
								goto l52
							l54:
								position, tokenIndex = position52, tokenIndex52
								if buffer[position] != rune('Q') {
									goto l55
								}
								position++
								goto l52
							l55:
								position, tokenIndex = position52, tokenIndex52
								if buffer[position] != rune('q') {
									goto l51
								}
								position++
							}
						l52:
							goto l50
						l51:
							position, tokenIndex = position51, tokenIndex51
						}
					}
				l48:
					begin := position47
					end := position
					text = string(buffer[begin:end])
				}
				if !_rules[ruleAction3]() {
					goto l45
				}
				add(rulecastlingAbility, position46)
			}
			return true
		l45:
			position, tokenIndex = position45, tokenIndex45
			return false
		},
		/* 5 enPassantSquare <- <(<('-' / (F R))> Action4)> */
		func() bool {
			position56, tokenIndex56 := position, tokenIndex
			{
				position57 := position
				{
					position58 := position
					{
						position59, tokenIndex59 := position, tokenIndex
						if buffer[position] != rune('-') {
							goto l60
						}
						position++
						goto l59
					l60:
						position, tokenIndex = position59, tokenIndex59
						if !_rules[ruleF]() {
							goto l56
						}
						if !_rules[ruleR]() {
							goto l56
						}
					}
				l59:
					begin := position58
					end := position
					text = string(buffer[begin:end])
				}
				if !_rules[ruleAction4]() {
					goto l56
				}
				add(ruleenPassantSquare, position57)
			}
			return true
		l56:
			position, tokenIndex = position56, tokenIndex56
			return false
		},
		/* 6 operation <- <(operationID / operationBm / operationAm / operationCe / operationFmvn / operationHmvc / operationComment)> */
		func() bool {
			position61, tokenIndex61 := position, tokenIndex
			{
				position62 := position
				{
					position63, tokenIndex63 := position, tokenIndex
					if !_rules[ruleoperationID]() {
						goto l64
					}
					goto l63
				l64:
					position, tokenIndex = position63, tokenIndex63
					if !_rules[ruleoperationBm]() {
						goto l65
					}
					goto l63
				l65:
					position, tokenIndex = position63, tokenIndex63
					if !_rules[ruleoperationAm]() {
						goto l66
					}
					goto l63
				l66:
					position, tokenIndex = position63, tokenIndex63
					if !_rules[ruleoperationCe]() {
						goto l67
					}
					goto l63
				l67:
					position, tokenIndex = position63, tokenIndex63
					if !_rules[ruleoperationFmvn]() {
						goto l68
					}
					goto l63
				l68:
					position, tokenIndex = position63, tokenIndex63
					if !_rules[ruleoperationHmvc]() {
						goto l69
					}
					goto l63
				l69:
					position, tokenIndex = position63, tokenIndex63
					if !_rules[ruleoperationComment]() {
						goto l61
					}
				}
			l63:
				add(ruleoperation, position62)
			}
			return true
		l61:
			position, tokenIndex = position61, tokenIndex61
			return false
		},
		/* 7 operationID <- <('i' 'd' SPACE TEXT Action5)> */
		func() bool {
			position70, tokenIndex70 := position, tokenIndex
			{
				position71 := position
				if buffer[position] != rune('i') {
					goto l70
				}
				position++
				if buffer[position] != rune('d') {
					goto l70
				}
				position++
				if !_rules[ruleSPACE]() {
					goto l70
				}
				if !_rules[ruleTEXT]() {
					goto l70
				}
				if !_rules[ruleAction5]() {
					goto l70
				}
				add(ruleoperationID, position71)
			}
			return true
		l70:
			position, tokenIndex = position70, tokenIndex70
			return false
		},
		/* 8 operationBm <- <('b' 'm' (SPACE MOVE)* Action6)> */
		func() bool {
			position72, tokenIndex72 := position, tokenIndex
			{
				position73 := position
				if buffer[position] != rune('b') {
					goto l72
				}
				position++
				if buffer[position] != rune('m') {
					goto l72
				}
				position++
			l74:
				{
					position75, tokenIndex75 := position, tokenIndex
					if !_rules[ruleSPACE]() {
						goto l75
					}
					if !_rules[ruleMOVE]() {
						goto l75
					}
					goto l74
				l75:
					position, tokenIndex = position75, tokenIndex75
				}
				if !_rules[ruleAction6]() {
					goto l72
				}
				add(ruleoperationBm, position73)
			}
			return true
		l72:
			position, tokenIndex = position72, tokenIndex72
			return false
		},
		/* 9 operationAm <- <('a' 'm' (SPACE MOVE)* Action7)> */
		func() bool {
			position76, tokenIndex76 := position, tokenIndex
			{
				position77 := position
				if buffer[position] != rune('a') {
					goto l76
				}
				position++
				if buffer[position] != rune('m') {
					goto l76
				}
				position++
			l78:
				{
					position79, tokenIndex79 := position, tokenIndex
					if !_rules[ruleSPACE]() {
						goto l79
					}
					if !_rules[ruleMOVE]() {
						goto l79
					}
					goto l78
				l79:
					position, tokenIndex = position79, tokenIndex79
				}
				if !_rules[ruleAction7]() {
					goto l76
				}
				add(ruleoperationAm, position77)
			}
			return true
		l76:
			position, tokenIndex = position76, tokenIndex76
			return false
		},
		/* 10 operationFmvn <- <('f' 'm' 'v' 'n' SPACE NUMBER Action8)> */
		func() bool {
			position80, tokenIndex80 := position, tokenIndex
			{
				position81 := position
				if buffer[position] != rune('f') {
					goto l80
				}
				position++
				if buffer[position] != rune('m') {
					goto l80
				}
				position++
				if buffer[position] != rune('v') {
					goto l80
				}
				position++
				if buffer[position] != rune('n') {
					goto l80
				}
				position++
				if !_rules[ruleSPACE]() {
					goto l80
				}
				if !_rules[ruleNUMBER]() {
					goto l80
				}
				if !_rules[ruleAction8]() {
					goto l80
				}
				add(ruleoperationFmvn, position81)
			}
			return true
		l80:
			position, tokenIndex = position80, tokenIndex80
			return false
		},
		/* 11 operationHmvc <- <('h' 'm' 'v' 'c' SPACE NUMBER Action9)> */
		func() bool {
			position82, tokenIndex82 := position, tokenIndex
			{
				position83 := position
				if buffer[position] != rune('h') {
					goto l82
				}
				position++
				if buffer[position] != rune('m') {
					goto l82
				}
				position++
				if buffer[position] != rune('v') {
					goto l82
				}
				position++
				if buffer[position] != rune('c') {
					goto l82
				}
				position++
				if !_rules[ruleSPACE]() {
					goto l82
				}
				if !_rules[ruleNUMBER]() {
					goto l82
				}
				if !_rules[ruleAction9]() {
					goto l82
				}
				add(ruleoperationHmvc, position83)
			}
			return true
		l82:
			position, tokenIndex = position82, tokenIndex82
			return false
		},
		/* 12 operationComment <- <(commentName SPACE TEXT Action10)> */
		func() bool {
			position84, tokenIndex84 := position, tokenIndex
			{
				position85 := position
				if !_rules[rulecommentName]() {
					goto l84
				}
				if !_rules[ruleSPACE]() {
					goto l84
				}
				if !_rules[ruleTEXT]() {
					goto l84
				}
				if !_rules[ruleAction10]() {
					goto l84
				}
				add(ruleoperationComment, position85)
			}
			return true
		l84:
			position, tokenIndex = position84, tokenIndex84
			return false
		},
		/* 13 operationCe <- <('c' 'e' SPACE NUMBER Action11)> */
		func() bool {
			position86, tokenIndex86 := position, tokenIndex
			{
				position87 := position
				if buffer[position] != rune('c') {
					goto l86
				}
				position++
				if buffer[position] != rune('e') {
					goto l86
				}
				position++
				if !_rules[ruleSPACE]() {
					goto l86
				}
				if !_rules[ruleNUMBER]() {
					goto l86
				}
				if !_rules[ruleAction11]() {
					goto l86
				}
				add(ruleoperationCe, position87)
			}
			return true
		l86:
			position, tokenIndex = position86, tokenIndex86
			return false
		},
		/* 14 commentName <- <(<('c' [0-9])> Action12)> */
		func() bool {
			position88, tokenIndex88 := position, tokenIndex
			{
				position89 := position
				{
					position90 := position
					if buffer[position] != rune('c') {
						goto l88
					}
					position++
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l88
					}
					position++
					begin := position90
					end := position
					text = string(buffer[begin:end])
				}
				if !_rules[ruleAction12]() {
					goto l88
				}
				add(rulecommentName, position89)
			}
			return true
		l88:
			position, tokenIndex = position88, tokenIndex88
			return false
		},
		/* 15 MOVE <- <(<(CASTLING / PAWN_MOVE / PIECE_MOVE)> (('+' / '=' / '!' / '?')* / ('e' '.' 'p' '.'))? Action13)> */
		func() bool {
			position91, tokenIndex91 := position, tokenIndex
			{
				position92 := position
				{
					position93 := position
					{
						position94, tokenIndex94 := position, tokenIndex
						if !_rules[ruleCASTLING]() {
							goto l95
						}
						goto l94
					l95:
						position, tokenIndex = position94, tokenIndex94
						if !_rules[rulePAWN_MOVE]() {
							goto l96
						}
						goto l94
					l96:
						position, tokenIndex = position94, tokenIndex94
						if !_rules[rulePIECE_MOVE]() {
							goto l91
						}
					}
				l94:
					begin := position93
					end := position
					text = string(buffer[begin:end])
				}
				{
					position97, tokenIndex97 := position, tokenIndex
					{
						position99, tokenIndex99 := position, tokenIndex
					l101:
						{
							position102, tokenIndex102 := position, tokenIndex
							{
								position103, tokenIndex103 := position, tokenIndex
								if buffer[position] != rune('+') {
									goto l104
								}
								position++
								goto l103
							l104:
								position, tokenIndex = position103, tokenIndex103
								if buffer[position] != rune('=') {
									goto l105
								}
								position++
								goto l103
							l105:
								position, tokenIndex = position103, tokenIndex103
								if buffer[position] != rune('!') {
									goto l106
								}
								position++
								goto l103
							l106:
								position, tokenIndex = position103, tokenIndex103
								if buffer[position] != rune('?') {
									goto l102
								}
								position++
							}
						l103:
							goto l101
						l102:
							position, tokenIndex = position102, tokenIndex102
						}
						goto l99

						position, tokenIndex = position99, tokenIndex99
						if buffer[position] != rune('e') {
							goto l97
						}
						position++
						if buffer[position] != rune('.') {
							goto l97
						}
						position++
						if buffer[position] != rune('p') {
							goto l97
						}
						position++
						if buffer[position] != rune('.') {
							goto l97
						}
						position++
					}
				l99:
					goto l98
				l97:
					position, tokenIndex = position97, tokenIndex97
				}
			l98:
				if !_rules[ruleAction13]() {
					goto l91
				}
				add(ruleMOVE, position92)
			}
			return true
		l91:
			position, tokenIndex = position91, tokenIndex91
			return false
		},
		/* 16 PAWN_MOVE <- <(SQUARES '='? ('N' / 'B' / 'R' / 'Q')?)> */
		func() bool {
			position107, tokenIndex107 := position, tokenIndex
			{
				position108 := position
				if !_rules[ruleSQUARES]() {
					goto l107
				}
				{
					position109, tokenIndex109 := position, tokenIndex
					if buffer[position] != rune('=') {
						goto l109
					}
					position++
					goto l110
				l109:
					position, tokenIndex = position109, tokenIndex109
				}
			l110:
				{
					position111, tokenIndex111 := position, tokenIndex
					{
						position113, tokenIndex113 := position, tokenIndex
						if buffer[position] != rune('N') {
							goto l114
						}
						position++
						goto l113
					l114:
						position, tokenIndex = position113, tokenIndex113
						if buffer[position] != rune('B') {
							goto l115
						}
						position++
						goto l113
					l115:
						position, tokenIndex = position113, tokenIndex113
						if buffer[position] != rune('R') {
							goto l116
						}
						position++
						goto l113
					l116:
						position, tokenIndex = position113, tokenIndex113
						if buffer[position] != rune('Q') {
							goto l111
						}
						position++
					}
				l113:
					goto l112
				l111:
					position, tokenIndex = position111, tokenIndex111
				}
			l112:
				add(rulePAWN_MOVE, position108)
			}
			return true
		l107:
			position, tokenIndex = position107, tokenIndex107
			return false
		},
		/* 17 PIECE_MOVE <- <(('N' / 'B' / 'R' / 'Q' / 'K') SQUARES)> */
		func() bool {
			position117, tokenIndex117 := position, tokenIndex
			{
				position118 := position
				{
					position119, tokenIndex119 := position, tokenIndex
					if buffer[position] != rune('N') {
						goto l120
					}
					position++
					goto l119
				l120:
					position, tokenIndex = position119, tokenIndex119
					if buffer[position] != rune('B') {
						goto l121
					}
					position++
					goto l119
				l121:
					position, tokenIndex = position119, tokenIndex119
					if buffer[position] != rune('R') {
						goto l122
					}
					position++
					goto l119
				l122:
					position, tokenIndex = position119, tokenIndex119
					if buffer[position] != rune('Q') {
						goto l123
					}
					position++
					goto l119
				l123:
					position, tokenIndex = position119, tokenIndex119
					if buffer[position] != rune('K') {
						goto l117
					}
					position++
				}
			l119:
				if !_rules[ruleSQUARES]() {
					goto l117
				}
				add(rulePIECE_MOVE, position118)
			}
			return true
		l117:
			position, tokenIndex = position117, tokenIndex117
			return false
		},
		/* 18 CASTLING <- <((('o' / 'O') '-' ('o' / 'O') '-' ('o' / 'O')) / (('o' / 'O') '-' ('o' / 'O')))> */
		func() bool {
			position124, tokenIndex124 := position, tokenIndex
			{
				position125 := position
				{
					position126, tokenIndex126 := position, tokenIndex
					{
						position128, tokenIndex128 := position, tokenIndex
						if buffer[position] != rune('o') {
							goto l129
						}
						position++
						goto l128
					l129:
						position, tokenIndex = position128, tokenIndex128
						if buffer[position] != rune('O') {
							goto l127
						}
						position++
					}
				l128:
					if buffer[position] != rune('-') {
						goto l127
					}
					position++
					{
						position130, tokenIndex130 := position, tokenIndex
						if buffer[position] != rune('o') {
							goto l131
						}
						position++
						goto l130
					l131:
						position, tokenIndex = position130, tokenIndex130
						if buffer[position] != rune('O') {
							goto l127
						}
						position++
					}
				l130:
					if buffer[position] != rune('-') {
						goto l127
					}
					position++
					{
						position132, tokenIndex132 := position, tokenIndex
						if buffer[position] != rune('o') {
							goto l133
						}
						position++
						goto l132
					l133:
						position, tokenIndex = position132, tokenIndex132
						if buffer[position] != rune('O') {
							goto l127
						}
						position++
					}
				l132:
					goto l126
				l127:
					position, tokenIndex = position126, tokenIndex126
					{
						position134, tokenIndex134 := position, tokenIndex
						if buffer[position] != rune('o') {
							goto l135
						}
						position++
						goto l134
					l135:
						position, tokenIndex = position134, tokenIndex134
						if buffer[position] != rune('O') {
							goto l124
						}
						position++
					}
				l134:
					if buffer[position] != rune('-') {
						goto l124
					}
					position++
					{
						position136, tokenIndex136 := position, tokenIndex
						if buffer[position] != rune('o') {
							goto l137
						}
						position++
						goto l136
					l137:
						position, tokenIndex = position136, tokenIndex136
						if buffer[position] != rune('O') {
							goto l124
						}
						position++
					}
				l136:
				}
			l126:
				add(ruleCASTLING, position125)
			}
			return true
		l124:
			position, tokenIndex = position124, tokenIndex124
			return false
		},
		/* 19 SQUARES <- <((F R ('-' / 'x')? F R) / (F ('-' / 'x')? F R) / (R ('-' / 'x')? F R) / ('x'? F R))> */
		func() bool {
			position138, tokenIndex138 := position, tokenIndex
			{
				position139 := position
				{
					position140, tokenIndex140 := position, tokenIndex
					if !_rules[ruleF]() {
						goto l141
					}
					if !_rules[ruleR]() {
						goto l141
					}
					{
						position142, tokenIndex142 := position, tokenIndex
						{
							position144, tokenIndex144 := position, tokenIndex
							if buffer[position] != rune('-') {
								goto l145
							}
							position++
							goto l144
						l145:
							position, tokenIndex = position144, tokenIndex144
							if buffer[position] != rune('x') {
								goto l142
							}
							position++
						}
					l144:
						goto l143
					l142:
						position, tokenIndex = position142, tokenIndex142
					}
				l143:
					if !_rules[ruleF]() {
						goto l141
					}
					if !_rules[ruleR]() {
						goto l141
					}
					goto l140
				l141:
					position, tokenIndex = position140, tokenIndex140
					if !_rules[ruleF]() {
						goto l146
					}
					{
						position147, tokenIndex147 := position, tokenIndex
						{
							position149, tokenIndex149 := position, tokenIndex
							if buffer[position] != rune('-') {
								goto l150
							}
							position++
							goto l149
						l150:
							position, tokenIndex = position149, tokenIndex149
							if buffer[position] != rune('x') {
								goto l147
							}
							position++
						}
					l149:
						goto l148
					l147:
						position, tokenIndex = position147, tokenIndex147
					}
				l148:
					if !_rules[ruleF]() {
						goto l146
					}
					if !_rules[ruleR]() {
						goto l146
					}
					goto l140
				l146:
					position, tokenIndex = position140, tokenIndex140
					if !_rules[ruleR]() {
						goto l151
					}
					{
						position152, tokenIndex152 := position, tokenIndex
						{
							position154, tokenIndex154 := position, tokenIndex
							if buffer[position] != rune('-') {
								goto l155
							}
							position++
							goto l154
						l155:
							position, tokenIndex = position154, tokenIndex154
							if buffer[position] != rune('x') {
								goto l152
							}
							position++
						}
					l154:
						goto l153
					l152:
						position, tokenIndex = position152, tokenIndex152
					}
				l153:
					if !_rules[ruleF]() {
						goto l151
					}
					if !_rules[ruleR]() {
						goto l151
					}
					goto l140
				l151:
					position, tokenIndex = position140, tokenIndex140
					{
						position156, tokenIndex156 := position, tokenIndex
						if buffer[position] != rune('x') {
							goto l156
						}
						position++
						goto l157
					l156:
						position, tokenIndex = position156, tokenIndex156
					}
				l157:
					if !_rules[ruleF]() {
						goto l138
					}
					if !_rules[ruleR]() {
						goto l138
					}
				}
			l140:
				add(ruleSQUARES, position139)
			}
			return true
		l138:
			position, tokenIndex = position138, tokenIndex138
			return false
		},
		/* 20 F <- <[a-h]> */
		func() bool {
			position158, tokenIndex158 := position, tokenIndex
			{
				position159 := position
				if c := buffer[position]; c < rune('a') || c > rune('h') {
					goto l158
				}
				position++
				add(ruleF, position159)
			}
			return true
		l158:
			position, tokenIndex = position158, tokenIndex158
			return false
		},
		/* 21 R <- <[1-8]> */
		func() bool {
			position160, tokenIndex160 := position, tokenIndex
			{
				position161 := position
				if c := buffer[position]; c < rune('1') || c > rune('8') {
					goto l160
				}
				position++
				add(ruleR, position161)
			}
			return true
		l160:
			position, tokenIndex = position160, tokenIndex160
			return false
		},
		/* 22 SPACE <- <' '+> */
		func() bool {
			position162, tokenIndex162 := position, tokenIndex
			{
				position163 := position
				if buffer[position] != rune(' ') {
					goto l162
				}
				position++
			l164:
				{
					position165, tokenIndex165 := position, tokenIndex
					if buffer[position] != rune(' ') {
						goto l165
					}
					position++
					goto l164
				l165:
					position, tokenIndex = position165, tokenIndex165
				}
				add(ruleSPACE, position163)
			}
			return true
		l162:
			position, tokenIndex = position162, tokenIndex162
			return false
		},
		/* 23 NUMBER <- <(<(('+' / '-')? ('0' / ([1-9] [0-9]*)))> Action14)> */
		func() bool {
			position166, tokenIndex166 := position, tokenIndex
			{
				position167 := position
				{
					position168 := position
					{
						position169, tokenIndex169 := position, tokenIndex
						{
							position171, tokenIndex171 := position, tokenIndex
							if buffer[position] != rune('+') {
								goto l172
							}
							position++
							goto l171
						l172:
							position, tokenIndex = position171, tokenIndex171
							if buffer[position] != rune('-') {
								goto l169
							}
							position++
						}
					l171:
						goto l170
					l169:
						position, tokenIndex = position169, tokenIndex169
					}
				l170:
					{
						position173, tokenIndex173 := position, tokenIndex
						if buffer[position] != rune('0') {
							goto l174
						}
						position++
						goto l173
					l174:
						position, tokenIndex = position173, tokenIndex173
						if c := buffer[position]; c < rune('1') || c > rune('9') {
							goto l166
						}
						position++
					l175:
						{
							position176, tokenIndex176 := position, tokenIndex
							if c := buffer[position]; c < rune('0') || c > rune('9') {
								goto l176
							}
							position++
							goto l175
						l176:
							position, tokenIndex = position176, tokenIndex176
						}
					}
				l173:
					begin := position168
					end := position
					text = string(buffer[begin:end])
				}
				if !_rules[ruleAction14]() {
					goto l166
				}
				add(ruleNUMBER, position167)
			}
			return true
		l166:
			position, tokenIndex = position166, tokenIndex166
			return false
		},
		/* 24 TEXT <- <('"' <(!'"' .)+> '"' Action15)> */
		func() bool {
			position177, tokenIndex177 := position, tokenIndex
			{
				position178 := position
				if buffer[position] != rune('"') {
					goto l177
				}
				position++
				{
					position179 := position
					{
						position182, tokenIndex182 := position, tokenIndex
						if buffer[position] != rune('"') {
							goto l182
						}
						position++
						goto l177
					l182:
						position, tokenIndex = position182, tokenIndex182
					}
					if !matchDot() {
						goto l177
					}
				l180:
					{
						position181, tokenIndex181 := position, tokenIndex
						{
							position183, tokenIndex183 := position, tokenIndex
							if buffer[position] != rune('"') {
								goto l183
							}
							position++
							goto l181
						l183:
							position, tokenIndex = position183, tokenIndex183
						}
						if !matchDot() {
							goto l181
						}
						goto l180
					l181:
						position, tokenIndex = position181, tokenIndex181
					}
					begin := position179
					end := position
					text = string(buffer[begin:end])
				}
				if buffer[position] != rune('"') {
					goto l177
				}
				position++
				if !_rules[ruleAction15]() {
					goto l177
				}
				add(ruleTEXT, position178)
			}
			return true
		l177:
			position, tokenIndex = position177, tokenIndex177
			return false
		},
		/* 26 Action0 <- <{
		        p.epd = &notation.EPD{
		Position: board.NewPosition(),
		                  Comment: make(map[string]string),
		        }
		}> */
		func() bool {
			{

				p.epd = &notation.EPD{
					Position: board.NewPosition(),
					Comment:  make(map[string]string),
				}

			}
			return true
		},
		nil,
		/* 28 Action1 <- <{
		        if err := board.ParsePiecePlacement(text, p.epd.Position); err != nil {
		                p.err = err
		                        return true
		        }
		}> */
		func() bool {
			{

				if err := board.ParsePiecePlacement(text, p.epd.Position); err != nil {
					p.err = err
					return true
				}

			}
			return true
		},
		/* 29 Action2 <- <{
		        if err := board.ParseSideToMove(text, p.epd.Position); err != nil {
		                p.err = err
		                        return true
		        }
		}> */
		func() bool {
			{

				if err := board.ParseSideToMove(text, p.epd.Position); err != nil {
					p.err = err
					return true
				}

			}
			return true
		},
		/* 30 Action3 <- <{
		        if err := board.ParseCastlingAbility(text, p.epd.Position); err != nil {
		                p.err = err
		                        return true
		        }
		}> */
		func() bool {
			{

				if err := board.ParseCastlingAbility(text, p.epd.Position); err != nil {
					p.err = err
					return true
				}

			}
			return true
		},
		/* 31 Action4 <- <{
		        if err := board.ParseEnpassantSquare(text, p.epd.Position); err != nil {
		                p.err = err
		                        return true
		        }
		}> */
		func() bool {
			{

				if err := board.ParseEnpassantSquare(text, p.epd.Position); err != nil {
					p.err = err
					return true
				}

			}
			return true
		},
		/* 32 Action5 <- <{
		        p.epd.Id = p.text
		}> */
		func() bool {
			{

				p.epd.Id = p.text

			}
			return true
		},
		/* 33 Action6 <- <{
		        p.epd.BestMove = p.moves
		                p.moves = nil
		}> */
		func() bool {
			{

				p.epd.BestMove = p.moves
				p.moves = nil

			}
			return true
		},
		/* 34 Action7 <- <{
		        p.epd.AvoidMove = p.moves
		                p.moves = nil
		}> */
		func() bool {
			{

				p.epd.AvoidMove = p.moves
				p.moves = nil

			}
			return true
		},
		/* 35 Action8 <- <{
		        p.epd.Position.SetFullmoveCounter(p.number)
		}> */
		func() bool {
			{

				p.epd.Position.SetFullmoveCounter(p.number)

			}
			return true
		},
		/* 36 Action9 <- <{
		        p.epd.Position.SetHalfmoveClock(p.number)
		}> */
		func() bool {
			{

				p.epd.Position.SetHalfmoveClock(p.number)

			}
			return true
		},
		/* 37 Action10 <- <{
		        p.epd.Comment[p.comment] = p.text
		}> */
		func() bool {
			{

				p.epd.Comment[p.comment] = p.text

			}
			return true
		},
		/* 38 Action11 <- <{
		        p.epd.Evaluation = new(int32)
		                *p.epd.Evaluation = int32(p.number)
		}> */
		func() bool {
			{

				p.epd.Evaluation = new(int32)
				*p.epd.Evaluation = int32(p.number)

			}
			return true
		},
		/* 39 Action12 <- <{
		        p.comment = text
		}> */
		func() bool {
			{

				p.comment = text

			}
			return true
		},
		/* 40 Action13 <- <{
		        m, err := san.SANToMove(p.epd.Position, text)
		                if err != nil {
		                        p.err = err
		                                return true
		                }
		        p.moves = append(p.moves, m)
		}> */
		func() bool {
			{

				m, err := san.SANToMove(p.epd.Position, text)
				if err != nil {
					p.err = err
					return true
				}
				p.moves = append(p.moves, m)

			}
			return true
		},
		/* 41 Action14 <- <{
		        n, err := strconv.Atoi(text)
		                if err != nil {
		                        p.err = err
		                                return true
		                }
		        p.number = n
		}> */
		func() bool {
			{

				n, err := strconv.Atoi(text)
				if err != nil {
					p.err = err
					return true
				}
				p.number = n

			}
			return true
		},
		/* 42 Action15 <- <{
		        p.text = text
		}> */
		func() bool {
			{

				p.text = text

			}
			return true
		},
	}
	p.rules = _rules
}
