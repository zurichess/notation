// Copyright 2014-2016 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate peg epd.peg

// Package epd implementes a parser for chess positions in EPD format.
//
// For a description of EPD see http://www.thechessdrum.net/PGN_Reference.txt
package epd

import "bitbucket.org/zurichess/notation"

// Parser implements a parser for chess positions in epd notation.
type Parser struct{}

func (p *Parser) Parse(line string) (*notation.EPD, error) {
	parser := &pegEPD{Buffer: line}
	parser.Init()
	if err := parser.Parse(); err != nil {
		return nil, err
	}
	parser.Init()
	if parser.err != nil {
		return nil, parser.err
	}
	return parser.epd, nil
}
