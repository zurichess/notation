// Copyright 2014-2016 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go tool yacc -o epd_parser.go epd_parser.y

// Package notation implements parsing of chess positions.
//
// Current supported formats are FEN and EPD notations.
// For EPD format see https://chessprogramming.wikispaces.com/Extended+Position+Description.
// For FEN format see https://chessprogramming.wikispaces.com/Forsyth-Edwards+Notation.
package notation

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	. "bitbucket.org/zurichess/board"
)

// EPD contains a parsed chess position in Extended Position Description.
// https://chessprogramming.wikispaces.com/Extended+Position+Description
type EPD struct {
	Position   *Position         // first 4 fields
	Id         string            // id
	BestMove   []Move            // bm
	AvoidMove  []Move            // am
	Comment    map[string]string // c0-c9
	Evaluation *int32            // ce, centipawn evaluation if not nil
}

func (e *EPD) String() string {
	s := FormatPiecePlacement(e.Position)
	s += " " + FormatSideToMove(e.Position)
	s += " " + FormatCastlingAbility(e.Position)
	s += " " + FormatEnpassantSquare(e.Position)

	if len(e.AvoidMove) != 0 {
		s += " am"
		for _, am := range e.AvoidMove {
			s += " " + am.LAN()
		}
		s += ";"
	}
	if len(e.BestMove) != 0 {
		s += " bm"
		for _, bm := range e.BestMove {
			s += " " + bm.LAN()
		}
		s += ";"
	}
	for k, v := range e.Comment {
		s += " " + k + " \"" + v + "\";"
	}
	if e.Evaluation != nil {
		s += " ce " + strconv.Itoa(int(*e.Evaluation)) + ";"
	}
	if e.Position.HalfmoveClock() != 0 {
		s += " hmvc " + strconv.Itoa(e.Position.HalfmoveClock()) + ";"
	}
	if e.Position.FullmoveCounter() != 1 {
		s += " fmvn " + strconv.Itoa(e.Position.FullmoveCounter()) + ";"
	}
	if e.Id != "" {
		s += " id \"" + e.Id + "\";"
	}
	return s
}

// Parser parses a string and returns an EPD.
type Parser interface {
	Parse(line string) (*EPD, error)
}

// EPDScanner parses an input line by line and returns EPDs.
type EPDScanner struct {
	scanner *bufio.Scanner
	parser  Parser
	err     error
	epd     *EPD
}

// NewEPDScanner returns a new scanner that reads epds from file one by one.
func NewEPDScanner(r io.Reader, p Parser) *EPDScanner {
	return &EPDScanner{
		scanner: bufio.NewScanner(r),
		parser:  p,
	}
}

// Scan returns true if there was a new EPD read.
// If the EPD parsing erroneous then Scan returns true
// and sets the error.
func (es *EPDScanner) Scan() bool {
	for es.scanner.Scan() {
		line := es.scanner.Text()

		// Skip empty lines
		line = strings.SplitN(line, "#", 2)[0]
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}

		// Reads position from file.
		es.epd, es.err = es.parser.Parse(line)
		return true
	}

	es.err = es.scanner.Err()
	return false
}

// EPD returns the parsed position.
func (es *EPDScanner) EPD() *EPD {
	return es.epd
}

// Err returns the last error.
func (es *EPDScanner) Err() error {
	return es.err
}
